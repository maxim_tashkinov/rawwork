# -*- coding: utf8 -*-

import sys, os
import pylab as P
import numpy as np
from tifffile import TiffFile,  imsave

from skimage import filter, exposure

from usetiff import * 

import cProfile, pstats, StringIO
import pylab as P


pr = cProfile.Profile()

def deriviate(arr):
    res = []
    for x in xrange(1, len(arr)-1):
        res.append(arr[x] - arr[x-1])

    return res

def find_extremums(histogram):
    img_der = deriviate(histogram)
    #print "img_der \n", img_der
    max_der = max(img_der)
    min_der = min(np.abs(img_der)) # производная может быть и <0
    
    res_min = {}
    res_max = {}
    min_curr = max_der
    sign = 0
    print "max, min deriviate, d ", max_der, min_der, min_der + (max_der-min_der)*0.1 
    for x in xrange(1, len(img_der)-1):
        if np.abs(img_der[x]) < min_der + (max_der-min_der)*0.1 :
            if img_der[x] > 0 and img_der[x-1] < 0: 
                sign = -1 # с убывания на возрастание - минимум
                res_min[x] = histogram[x]

            elif img_der[x] < 0 and img_der[x-1] > 0 :#and img_der[x+1] < 0 : 
                sign = 1 # с возрастания на убывания - максимум
                res_max[x] = histogram[x]

            if min_curr > np.abs(img_der[x]): min_curr = img_der[x]

    #print "\n\nextr \n min:  ", res_min, "\n\nmax:  ", res_max, "\n\n min_curr", min_curr
    return res_min, res_max


def smoth(arr):
    arr_len = len(arr)
    res = {}
    resa = [arr[0]]
    i=0
    for x in xrange(1, arr_len):
        if abs(resa[i] - arr[x]) > 10:
          resa.append(arr[x])
          i+=1
          print "#", i, x, resa[i], arr[x] 
        else:
          print "-", i, x, resa[i], arr[x] 

    return resa




def select_minimums(extr_dict):
    values = extr_dict.values()
    max_v = max(values)
    print "max_v", max_v
    minimums = {}
    for key, value in extr_dict.items():
        if max_v*0.05 < value < max_v * 0.35:
            minimums[key] = value
            print '+', key, value
        else:
           # print '-', key, value
           pass
    return minimums

def select_maximums(extr_dict):
    values = extr_dict.values()
    max_v = max(values)
    print "max_v", max_v
    maximums = {}
    for key, value in extr_dict.items():
        if  value > max_v*0.6:
            maximums[key] = value
            print '+', key, value
        else:
           # print '-', key, value
           pass
    return maximums



def use_curves():
    pr.enable()
    print os.path.exists(sys.argv[1])
    if len(sys.argv)>1 and os.path.exists(sys.argv[1]):
        fn = sys.argv[1]
        print fn, '\n'
    else:
        fn = u'D:/IMG_5721.tiff'
        print sys.argv

    tif = TiffFile(fn)
    
    images = tif.asarray()
    corrected2 = TiffTo8Bit(skimage.exposure.adjust_log(images, 2))
    corrected4 = TiffTo8Bit(skimage.exposure.adjust_log(images, 4))
    #corrected_sigm = TiffTo8Bit(skimage.exposure.adjust_sigmoid(images, 0.5, 1))


    io.imsave('d:\corrected_log.png', corrected2)
    io.imsave('d:\corrected_log4.png', corrected4)
    #hist, bin_centers = skimage.exposure.histogram(corrected2, nbins=32)
    #print hist, '\n\n',bin_centers


    hist, bin_centers = skimage.exposure.histogram(corrected2, nbins=256)
    norm_histo = hist.astype(float) / hist.sum() # Probability mass function
    #print "norm_histo ", norm_histo
    P1 = np.cumsum(norm_histo) # Cumulative normalized histogram
    #print P1
    P1_sq = np.cumsum(norm_histo ** 2)
    #print "P1_sq ",P1_sq
    # Get cumsum calculated from end of squared array:
    P2_sq = np.cumsum(norm_histo[::-1] ** 2)[::-1]
    # P2_sq indexes is shifted +1. I assume, with P1[:-1] it's help avoid '-inf'
    # in crit. ImageJ Yen implementation replaces those values by zero.
    crit = np.log(((P1_sq[:-1] * P2_sq[1:]) ** -1) * \
                  (P1[:-1] * (1.0 - P1[:-1])) ** 2)
    max_crit = np.argmax(crit)
    min_crit = np.argmin(crit)
    print "max_crit ", max_crit, "min crit ",  min_crit
    threshold = bin_centers[:-1][max_crit]
    print threshold
    #print corrected2[0][0:10]
    #img_mask =  corrected2 > threshold
    #img_mask2 = corrected2 < threshold
    #img_t = corrected2 * img_mask.astype(int)
    #img_t2 = corrected4 * img_mask2.astype(int)

    
    #io.imsave('d:\corrected_t.png', img_t)
    #io.imsave('d:\corrected_t2.png', img_t2)
    tif.close()

    #P.plot(crit)

   
    der = deriviate(hist)
    extr_min, extr_max = find_extremums(hist)

    print "min ", extr_min
    print "max ", extr_max
    minimums = select_minimums(extr_min)
    maximums = select_maximums(extr_min)

    keys = list(minimums.keys())

    keys.sort()
    s_keys = smoth(keys)
    mmin_1 = min(s_keys)
    print "min min ", mmin_1

    mmin_2 =s_keys[-1]

    print "min min ", mmin_2, keys
 
    #print corrected2[0][0:10]
    img_mask =  corrected2 > mmin_1
    img_t = corrected2 * img_mask.astype(int)

    img_mask2 =  corrected2 < mmin_1
    img_t2 = corrected2 * img_mask2.astype(int)

    img_mask3 =  corrected2 > mmin_2
    img_t3 = corrected4 * img_mask3.astype(int)


    k1 = 255 / mmin_1
    k2 = 255 / mmin_2
    
    corrected_new = TiffTo8Bit(skimage.exposure.adjust_log(images, k1))  
    corrected_new2 = TiffTo8Bit(skimage.exposure.adjust_log(images, k2))  

    io.imsave('d:\corrected_m.png', img_t)
    io.imsave('d:\corrected_m2.png', img_t2)
    io.imsave('d:\corrected_m3.png', img_t3)

    io.imsave('d:\corrected_m4.png', corrected_new)
    io.imsave('d:\corrected_m5.png', corrected_new2)
 



    P.plot(hist)
    P.plot(der)
    #P.plot(extr_min.values(), extr_min.keys())

    P.show()


    pr.disable()
    s = StringIO.StringIO()
    sortby = 'cumulative'
    ps = pstats.Stats(pr, stream=s).sort_stats(sortby)
    ps.print_stats()
    #print s.getvalue()


def main():
    use_curves()

if __name__ == "__main__": main()
