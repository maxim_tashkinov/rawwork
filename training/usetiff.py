# -*- coding: utf8 -*-
from tifffile import TiffFile,  imsave
import sys, os

from numpy import amax, amin, zeros
import numpy
from skimage import io, filter
from skimage.exposure import adjust_log
from scipy import ndimage
import matplotlib.pyplot as plt
import pylab as P
import skimage



def rgbFromTiff(data, k=1):
    shape = data.shape
    h,w,b = shape
    
    
    res = zeros((h,w,b), int)
    print 'rgbFromTiff', shape, 'gain k=', k
    for i in xrange(1, h-1):
        for y in xrange(1, w-1):
            res[i][y][0] = numpy.round(data[i][y][0] * k * 256.0/2**16)
            res[i][y][1] = numpy.round(data[i][y][1] * k * 256.0/2**16)
            res[i][y][2] = numpy.round(data[i][y][2] * k * 256.0/2**16)
                      
    return res

def TiffTo8Bit(data):
    shape = data.shape
    print 'TiffTo8Bit', shape
    res = skimage.exposure.rescale_intensity(data, None, (0, 255))
                     
    return res



def rgbFromTiffLimit(data, k=1):
    shape = data.shape
    h,w,b = shape
    
    
    res = zeros((h,w,b), int)
    print 'rgbFromTiff', shape, 'gain k=', k
    for i in xrange(1, h-1):
        for y in xrange(1, w-1):          
            res[i][y][0] = numpy.round(data[i][y][0] * k * 256.0/2**16)
            res[i][y][1] = numpy.round(data[i][y][1] * k * 256.0/2**16)
            res[i][y][2] = numpy.round(data[i][y][2] * k * 256.0/2**16)
            if res[i][y][0] >=255 or res[i][y][1] >= 255 or res[i][y][2] >= 255:
                res[i][y][0] = 0
                res[i][y][1] = 0
                res[i][y][2] = 0
                      
    return res


#

def rgbFromTiffLimit2(data, k=1):
    shape = data.shape
    h,w,b = shape
    
    
    res = zeros((h,w,b), int)
    print 'rgbFromTiff', shape, 'gain k=', k
    for i in xrange(1, h-1):
        for y in xrange(1, w-1):          
            res[i][y][0] = data[i][y][0] * k #* 256.0/2**16
            res[i][y][1] = data[i][y][1] * k #* 256.0/2**16
            res[i][y][2] = data[i][y][2] * k #* 256.0/2**16
            if res[i][y][0] >=65536 or res[i][y][1] >= 65536 or res[i][y][2] >= 65536:
                res[i][y][0] = 0
                res[i][y][1] = 0
                res[i][y][2] = 0
                      
    return skimage.img_as_ubyte(res)

def getGrad(data):

    shape = data.shape
    h,w,b = shape
    res = zeros((h,w,3), int)
    print shape
    for i in xrange(1, h-2):
        for y in xrange(1, w-2):
            grad = 65000/((data[i][y][1] - data[i][y-1][1]) + ((data[i+1][y+1][1] - data[i-1][y-1][1]) ))
            
            res[i][y] = (grad, grad,grad)

    return res


def correctDark(data, limit, k):
    shape = data.shape
    h,w,b = shape
    h = h/20
    w = w/20
    res = zeros((h,w,3), int)
    print 'shape', shape
    for i in xrange(1, h-1):
        for y in xrange(1, w-1):
            pass
            if numpy.sum(data[i][y])/3 < limit:
                res[i][y][0] = data[i][y][0]#numpy.round(data[i][y][0] * k * 256.0/2**16)
                res[i][y][1] = data[i][y][1]#numpy.round(data[i][y][1] * k * 256.0/2**16)
                res[i][y][2] = data[i][y][2]#numpy.round(data[i][y][2] * k * 256.0/2**16)
                #res[i][y] = numpy.round(numpy.dot(data[i][y],k))
                #res[i][y] = numpy.round(numpy.dot(res[i][y],256.0/2**16))
            else:
                res[i][y] = [0,0,0]#data[i][y]# numpy.round(numpy.dot(res[i][y], 256.0/2**16))
                
    return res




def getHistogramZones(data, show=False):
    # Функция строит гистограмму RGB и выделяет минимумы в ней
    zones={}
    print 2
    bb = 2**16   
    hgg=zeros((2**16,1), int)
    hgr=zeros((2**16,1), int)
    hgb=zeros((2**16,1), int)
    print 3
    shape = data.shape
    h,w,b = shape
    res = zeros((h,w,3), int)
    print 'getHistogramZones', shape
    for i in xrange(1, h-1):
        for y in xrange(1, w-1):
            rv = data[i][y][0]
            hgr[rv] += 1

            rg = data[i][y][1]
            hgg[rg] += 1

            rb = data[i][y][2]
            hgb[rb] += 1

    rmax = amax(hgr)
    grmax = amax(hgg)
    bmax = amax(hgb)
    print ' Rmax = %d Gmax=%d Bmax=%d' % (rmax, grmax, bmax)
    psr, psg, psb = 0,0,0
    ppsr, ppsg, ppsb = 0,0,0
    minimum_r = []


    absmax = max(rmax, grmax, bmax)
    print 'absmax', absmax, bb/absmax
    

    #top_edge = bb;
    #rvi=-1
    
    #for rvi in xrange(absmax, 1):
    #    print hgr[rvi], hgr[rvi-1]
    #    if hgr[rvi] < 100 and hgr[rvi-1] > 100:
    #        top_edge = rvi
    #        break


    #print 'found R max rvi=', top_edge
            
    


    part_size = 512
    part_count = bb / part_size
    
    for x in xrange(0, part_count):
        x_real = x*part_size
        sr = numpy.sum(hgr[x_real:(x+1)*part_size])
        sg = numpy.sum(hgg[x_real:(x+1)*part_size])
        sb = numpy.sum(hgb[x_real:(x+1)*part_size])
        print x_real, sr, sg,sb
        
        if (100< sr < part_size or 100 < sg < part_size or 100< sb < part_size):
            bb = x
            #print 'break at ', x, x_real
            break

        if max(sr,sg,sb) < part_size*absmax*0.08:
            #print 'SR < rmax', x, x_real, sr, part_size*rmax*0.08
            if psr > sr:
                print '\tpsr > sr'
                minimum_r.append(x_real) 

            

        psr, psg, psb =  sr, sg, sb
        
    print 'bb',bb
    print 'am', amax(hgb)

    print 'minimum_r', minimum_r
    limit = 0

    for mi in xrange(0, len(minimum_r)):
        m = minimum_r[mi]
        if m < bb*part_size/2:
            print m
            limit = m
                  
    
    top = bb*256
    print "top ", top

    

 
    #n, bins, patches = P.hist(hgb, 50, normed=1, histtype='bar')
    if show:
        P.plot(hgb)
        P.plot(hgg)
        P.plot(hgr)
       
        P.show()
     
    return limit, minimum_r, (hgr, hgg, hgb)#zones






def main():
    print 'use sk-test.py'

if __name__ == "__main__": main()
