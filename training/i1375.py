#! -*- coding:utf8 -*-
import sys, os
from skimage import io
from skimage.filter import denoise_tv_chambolle, denoise_bilateral
from skimage.exposure import equalize_hist
from skimage import exposure


def brightness(val):
    if val <120:    
          return val * 12 / 10
    else:
        return val



def main():
    if len(sys.argv)>1 and os.path.exists(sys.argv[1]):
        fn = sys.argv[1];
    else:
        fn = u'D:/my/Февраль/22 февраля/IMG_1375.JPG'


    image = io.imread(fn)
    c2 = image.copy()
    w,h, d = c2.shape
    print w, h, d

    H = exposure.histogram(image, nbins=2)

    print H
    
    for x in range(0, w):
        for y in range(0, h):
            if c2[x,y,0] > (c2[x,y,1] + c2[x,y,2])/2:
                c2[x,y,0] = (c2[x,y,1] + c2[x,y,2])/2

            c2[x,y,0] = brightness(c2[x,y,0])    
            c2[x,y,1] = brightness(c2[x,y,1])
            c2[x,y,2] = brightness(c2[x,y,2])    
        

        if x % 100 == 0:
            print x, c2[x,100:110,0] #,'max value in line',  max(c2[x,:,0])

           
            
    io.imsave(u'D:/IMG_1375_PY.JPG', c2)

if __name__ == "__main__": main()
print 'end1'


