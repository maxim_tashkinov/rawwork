# -*- coding: utf8 -*-

import sys, os
import pylab as P

from tifffile import TiffFile,  imsave

from skimage import filter, exposure

from usetiff import * 


print dir(filter)

print dir(exposure.exposure)

import cProfile, pstats, StringIO
pr = cProfile.Profile()


def use_histogram():
    pr.enable()
    if len(sys.argv)>1 and os.path.exists(sys.argv[1]):
        fn = sys.argv[1];
    else:
        fn = u'D:/IMG_5721.tiff'



    tif = TiffFile(fn)

    
    images = tif.asarray()
    print fn# images[1][0:10]
    print 'size', len(images), len(images[0])
    maxV, minV, meanV = amax(images), amin(images), images.mean()
    print "MaxV = %d, MinV = %d, MeanV = %d" % (maxV, minV, meanV)
    limit, mins, histogramms = getHistogramZones(images)
    print "last min = ", mins[-1]
    print "minimums: ", mins
    k = 65536.0/(mins[-1])
    k = numpy.round(k)
    print 'K = ', k

    val = filter.threshold_otsu(images)
    mask = images < val
    print 'threshold_otsu', val
    
    io.imsave('d:\mask.png', mask)
    #corr_log = adjust_log(images, k)
    #print 'from tiff 1'
    #corrected = rgbFromTiffLimit(images, k)#correctDark(images, limit, 1)
    #print 'from tiff 2', corrected[1000:1002][40:60]
    #corrected2 = rgbFromTiffLimit2(images, k*2)
    #print 'from tiff 3',  corrected2[1000:1002][40:60]
    #corrected3 = rgbFromTiffLimit2(images, k*4)
    #print 'from tiff 4',  correcte3[1000:1002][40:60]
    #corrected4 = rgbFromTiffLimit2(images, k*16)
    
    corrected2 = rgbFromTiff(skimage.exposure.rescale_intensity(images, (0, mins[-1])))
    
    print 'write corrected '
    
    io.imsave('d:\corrected2.png', corrected2)
    #print 'write corrected 2'
    #io.imsave('d:\corrected.png', corrected)
    #print 'write corrected 3'
    #io.imsave('d:\corrected3.png', corrected3)
    #print 'write corrected 4'
    #io.imsave('d:\corrected4.png', corrected4)

        
         
    #except Exception as e:
    #    print(e)
    #finally:
    tif.close()

    pr.disable()
    s = StringIO.StringIO()
    sortby = 'cumulative'
    ps = pstats.Stats(pr, stream=s).sort_stats(sortby)
    ps.print_stats()
    print s.getvalue()


def main():
    use_histogram()

if __name__ == "__main__": main()
